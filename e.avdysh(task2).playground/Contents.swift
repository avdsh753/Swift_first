import UIKit

func makeBuffer() -> (String) -> Void {
    var str1 = ""
    return { value in
        if value.isEmpty {
            print(str1)
        } else {
            str1 += value
        }
    }
}

var buffer = makeBuffer()

buffer("Замыкание")
buffer(" использовать")
buffer(" нужно!")
buffer("")




func checkPrimeNumber (_ number: Int) -> Bool {
    var result = true
    if number == 2 || number == 3 {
        result = true
    } else if number < 2 {
        result = false
    } else if number > 3 {
        for index in 2 ... number / 2 {
            if number % index == 0 {
                result = false
            }
        }
    }
    return result
}

print(checkPrimeNumber(4))
