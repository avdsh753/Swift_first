import UIKit

class Shape {
    open func calculateArea() {
        fatalError("Method not implemented")
    }

    open func calculatePerimeter() {
        fatalError("Method not implemented")
    }
}


class Rectangle: Shape {
    private var height: Double
    private var widht: Double
    init (height: Double, widht: Double){
        self.height = height
        self.widht = widht
    }
    
    override func calculateArea(){
        let area = height*widht
        print("area = ",area)
    }
    
    override func calculatePerimeter() {
        let perimeter = 2 * (widht * height)
        print("perimeter = ", perimeter)
    }
}

class Circle: Shape {
    private var radius: Double
    
    init (radius: Double){
        self.radius = radius
    }
    
    override func calculateArea() {
        let area = Double.pi * radius * radius
        print("area = ", area)
    }
    
    override func calculatePerimeter() {
        let perimeter = 2 * Double.pi * radius
        print("perimeter = ", perimeter)
    }
    
}

class Square: Shape {
    private let side: Double
    init (side: Double){
        self.side = side
    }
    
    override func calculateArea() {
        let area = side*side
        print("area = ", area)
    }
    
    override func calculatePerimeter() {
        let perimeter = 4 * side
        print("perimeter = ", perimeter)
    }
}
    
let shapes: [Shape] = [Rectangle(height: 10, widht: 20), Circle(radius: 10), Square(side: 10)]

for shape in shapes {
    shape.calculateArea()
    shape.calculatePerimeter()
    }

//part2

print("=====Task 2=====")


func findIndexWithGenerics<T: Equatable>(ofValue valueToFind: T, in array: [T]) -> Int? {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
if let foundIndex = findIndexWithGenerics(ofValue: "лама", in: arrayOfString) {
    print("Индекс ламы: ", foundIndex)
}

let arrayOfDouble = [1.1, 2.2, 3.3, 4.4, 5.5]
if let foundIndex = findIndexWithGenerics(ofValue: 3.3, in: arrayOfDouble) {
    print("Индекс числа 3.3: ", foundIndex)
}

let arrayOfInt = [1, 2, 3, 4, 5]
if let foundIndex = findIndexWithGenerics(ofValue: 4, in: arrayOfInt) {
    print("Индекс числа 4: ", foundIndex)
}
