import UIKit

var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

let milkPrice: Int = 3

/для задания 3/
let milkPrice3: Double = 4.20

/для задания 4/

let milkBottleCount: Int?

milkBottleCount = 20

var profit: Double = 0.0

if milkBottleCount == nil {
    print ("переменная milkBottleCount == nil")
} else {
    profit = milkPrice3 * Double(milkBottleCount!)
}
print(profit)

var employeesList: Array<String> = []

employeesList.append("Лактозов Иван")
employeesList.append("Рысев Петр")
employeesList.append("Воронцов Геннадий")
employeesList.append("Бондаренко Юрий")
employeesList.append("Авдыш Евгений")

print(employeesList)

var isEveryoneWorkHard: Bool = false
var workingHours: Double = 35
if (workingHours>=40){
    isEveryoneWorkHard = true
} else { isEveryoneWorkHard = false
}
print (isEveryoneWorkHard)

