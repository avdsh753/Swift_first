import UIKit

struct Device {
    
    static var iPhone14Pro: Device {return Device(name: "iPhone14Pro", screenSize: CGSize (width: 393, height: 852),  screenDiagonal: 6.1, scaleFactor: 3 )}
    
    static var iPhoneXR: Device {return Device(name: "iPhone XR", screenSize: CGSize (width: 414, height: 896),  screenDiagonal: 6.06, scaleFactor: 2 )}
    
    static var iPadPro: Device {return Device(name: "iPadPro", screenSize: CGSize (width: 834, height: 1194),  screenDiagonal: 11, scaleFactor: 2 )}
    
    var name: String
    var screenSize: CGSize
    var screenDiagonal: Double
    var scaleFactor: Int
     
    func physicalSize() -> CGSize {
        return CGSize(
            width: screenSize.width * CGFloat(scaleFactor),
            height: screenSize.height * CGFloat(scaleFactor)
        )
    }
}

print (Device.iPhoneXR.physicalSize())
